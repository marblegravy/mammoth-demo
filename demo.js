(function() {
    document.getElementById("document")
        .addEventListener("change", handleFileSelect, false);

    var options = {
        styleMap: [
            "p[style-name^='Custom'] => aside.shiny-box > h2",
            "r[style-name='ModifiedDate'] => small"
        ]
    };
    /* print the default options to the page on load */
    styleMap.value = JSON.stringify(options.styleMap, null, '\t');

    function getOptions() {
        console.log(JSON.parse(styleMap.value), options);
        options.styleMap = JSON.parse(styleMap.value);
        return options;
    }

    function handleFileSelect(event) {
        readFileInputEventAsArrayBuffer(event, function(arrayBuffer) {
            mammoth.convertToHtml({arrayBuffer: arrayBuffer}, getOptions())
                .then(displayResult)
                .done();
        });
    }

    function displayResult(result) {
        document.getElementById("output").innerHTML = result.value;

        var messageHtml = result.messages.map(function(message) {
            return '<li class="' + message.type + '">' + escapeHtml(message.message) + "</li>";
        }).join("");

        document.getElementById("messages").innerHTML = "<ul>" + messageHtml + "</ul>";
    }

    function readFileInputEventAsArrayBuffer(event, callback) {
        var file = event.target.files[0];

        var reader = new FileReader();

        reader.onload = function(loadEvent) {
            var arrayBuffer = loadEvent.target.result;
            callback(arrayBuffer);
        };

        reader.readAsArrayBuffer(file);
    }

    function escapeHtml(value) {
        return value
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }
})();
