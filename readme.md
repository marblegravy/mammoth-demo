# Document converter tool

This is an initial beta for a Word > HTML onversion tool designed to leverage the Mammoth.js plugin (https://github.com/mwilliamson/mammoth.js) to cleanly convert well marked up word documents in to sweet, sweet html.

## Roadmap
See https://bitbucket.org/marblegravy/mammoth-demo/issues?status=new&status=open 